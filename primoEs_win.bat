ECHO OFF
chcp 65001
cls
ECHO Git 4 dummies
ECHO Primo esercizio
MKDIR Es1
ECHO Hai configurato git? > Es1/domande.txt
ECHO. >> Es1/domande.txt
ECHO git è un sistema di versionamento centralizzato o distribuito? >> Es1/domande.txt
ECHO. >> Es1/domande.txt
ECHO Gitlab è un sistema di versionamento centralizzato o distibuito? >> Es1/domande.txt
ECHO. >> Es1/domande.txt
ECHO Spiegami, in due righe, il rapporto fra Working Directory e Staging Area. >> Es1/domande.txt
ECHO. >> Es1/domande.txt
ECHO Cosa succede quando diamo il comando "git commit"? >> Es1/domande.txt
ECHO. >> Es1/domande.txt
ECHO In questo esercizio ti farò alcune domande, alle quali dovrai dovrai rispondere.
ECHO Ti ho appena scritto le domande in un file apposito, si chiama Es1/domande.txt (che fantasia...).
ECHO Aprilo e scrivi la risposta sotto ad ogni domanda.
ECHO.
ECHO Buon lavoro :)
